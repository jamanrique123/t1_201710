package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getMin (IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int valorActual;

		if (bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();

			while (iter.hasNext())
			{
				valorActual = iter.next();
				if (valorActual < min)
				{
					min = valorActual;
				}
			}
		}

		return min;
	}

	public int getMedian(IntegersBag bag)
	{
		Iterator<Integer> iter = bag.getIterator();
		ArrayList<Integer> bagLista = new ArrayList<Integer>();  
		int median =0;

		if (bag != null)
		{

			while (iter.hasNext())
			{
				bagLista.add( iter.next() );
			}

			if (bagLista.size() % 2 == 0)
			{
				//Es par
				//La mediana para grupos pares, se define como el promedio entre los "dos elementos de la mitad" 
				int mitad= bagLista.size()/2;
				median = (bagLista.get(mitad) + bagLista.get(mitad+1))/2;

			}
			else
			{
				//como es impar, entonces cojo el del medio
				median = bagLista.get( (bagLista.size()/2) ); //como es impar, siempre tendr� xx.5 de argumento, pero como solamente recibe int's, recibe la parte entera, que es xx.
				//El elemento xx me sirve tal cual, debido a que la numeraci�n de los arreglos comienza desde 0
			}
		}
		
		return median;
	}

	public int getSumOf (IntegersBag bag)
	{
		int sum = 0;

		if (bag != null)
		{
			Iterator<Integer> iter =bag.getIterator();
			
			while (iter.hasNext())
			{
				sum += iter.next();
			}
		}
		
		return sum;
	}
}
